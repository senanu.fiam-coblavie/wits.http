﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WITS.Http
{
    public interface INetworkClientDelegate
    {
        string[] GetBadRequestMessages(string responseBody);

        Task OnBeforeRequest(HttpRequestMessage request);
        Task OnFailedRequest(Uri requestUri, HttpStatusCode statusCode, string reasonPhrase, HttpResponseHeaders responseHeaders, string responseBody);
    }
}
