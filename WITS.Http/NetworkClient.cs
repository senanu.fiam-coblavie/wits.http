﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WITS.Http.Failure;
using Microsoft.Extensions.Logging;
using WITS.Cache;
using System.Security.Cryptography;

namespace WITS.Http
{
    public class NetworkClient : INetworkClient
    {
        protected readonly HttpClient httpClient;
        protected readonly ILogger<NetworkClient> logger;
        protected readonly ICacheManager cacheManager;
        protected readonly INetworkClientDelegate deleg;

        public NetworkClient(HttpClient httpClient, ILogger<NetworkClient> logger, ICacheManager cacheManager, INetworkClientDelegate deleg = null)
        {
            this.httpClient = httpClient;
            this.logger = logger;
            this.cacheManager = cacheManager;
            this.deleg = deleg;

            if (!this.httpClient.DefaultRequestHeaders.Any(h => h.Key == "User-Agent"))
                this.httpClient.DefaultRequestHeaders.Add("User-Agent", "WITS NetworkClient/1.0 (CSharp)");
        }


        public async Task<T> Get<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, CacheOptions cacheOptions = null, int retries = 3)
        {
            var queryString = parameters == null ? "" : ((resource.IndexOf("?") == -1 ? "?" : "&") + string.Join("&", parameters.Select(p => $"{p.Key}={p.Value}")));

            if (!string.IsNullOrWhiteSpace(queryString))
                resource += queryString;
            var request = new HttpRequestMessage(HttpMethod.Get, resource);

            var cacheKey = GetCacheKey(HttpMethod.Get, resource);
            if (cacheOptions != null)
            {
                var cached = await cacheManager.Get<string>(cacheKey);

                if (cached != null)
                {
                    return JsonConvert.DeserializeObject<T>(cached); ;
                }
            }

            var response = await Execute(HttpMethod.Get, resource, null, retries);

            if (cacheOptions != null && response != null)
            {
                if (cacheOptions.ExpireBehaviour == CacheOptions.ExpiryBehaviour.Absolute)
                    await cacheManager.Set(cacheKey, response, DateTimeOffset.Now.AddSeconds(cacheOptions.ExpireSeconds));
                else
                    await cacheManager.Set(cacheKey, response, TimeSpan.FromSeconds(cacheOptions.ExpireSeconds));
            }

            return response == null ? default(T) : JsonConvert.DeserializeObject<T>(response);
        }

        public async Task Delete(string resource, int retries = 3)
        {
            await Execute(HttpMethod.Delete, resource, null, retries);
        }


        #region form

        public async Task<T> Post<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded, int retries = 0)
        {
            return await FormRequest<T>(HttpMethod.Put, resource, parameters, postMode, retries).ConfigureAwait(false);
        }

        public async Task<T> Put<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded, int retries = 0)
        {
            return await FormRequest<T>(HttpMethod.Put, resource, parameters, postMode, retries).ConfigureAwait(false);
        }

        public async Task<T> Patch<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded, int retries = 0)
        {
            return await FormRequest<T>(new HttpMethod("PATCH"), resource, parameters, postMode, retries).ConfigureAwait(false);
        }

        public async Task<T> Delete<T>(string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded, int retries = 0)
        {
            return await FormRequest<T>(HttpMethod.Delete, resource, parameters, postMode, retries).ConfigureAwait(false);
        }
        
        public async Task<T> FormRequest<T>(HttpMethod method, string resource, IEnumerable<KeyValuePair<string, string>> parameters = null, PostMode postMode = PostMode.FormUrlEncoded, int retries = 0)
        {
            var content = default(Func<HttpContent>);

            if (parameters != null)
            {
                if (postMode == PostMode.FormUrlEncoded)
                {
                    content = () => new FormUrlEncodedContent(parameters);
                }
                else
                {
                    content = () =>
                    {
                        var multipart = new MultipartFormDataContent();
                        foreach (var pair in parameters)
                        {
                            multipart.Add(new StringContent(pair.Value), String.Format("\"{0}\"", pair.Key));
                        }

                        return multipart;
                    };
                }
            }

            var response = await Execute(method, resource, content, retries).ConfigureAwait(false);
            return string.IsNullOrWhiteSpace(response) ? default(T) : JsonConvert.DeserializeObject<T>(response);
        }

        #endregion


        #region body

        public async Task<T> Post<T>(string resource, string content = null, string contentType = "application/json", int retries = 3)
        {
            return await BodyRequest<T>(HttpMethod.Post, resource, content, contentType = "application/json", retries).ConfigureAwait(false);
        }

        public async Task<T> Put<T>(string resource, string content = null, string contentType = "application/json", int retries = 3)
        {
            return await BodyRequest<T>(HttpMethod.Put, resource, content, contentType = "application/json", retries).ConfigureAwait(false);
        }

        public async Task<T> Patch<T>(string resource, string content = null, string contentType = "application/json", int retries = 3)
        {
            return await BodyRequest<T>(new HttpMethod("PATCH"), resource, content, contentType = "application/json", retries).ConfigureAwait(false);
        }

        public async Task<T> Delete<T>(string resource, string content = null, string contentType = "application/json", int retries = 3)
        {
            return await BodyRequest<T>(HttpMethod.Delete, resource, content, contentType = "application/json", retries).ConfigureAwait(false);
        }

        public async Task<T> BodyRequest<T>(HttpMethod method, string resource, string content = null, string contentType = "application/json", int retries = 3)
        {
            var content_ = default(Func<HttpContent>);

            if (content != null)
            {
                content_ = () =>
                {
                    return new StringContent(content, Encoding.UTF8, contentType);
                };

            }

            var response = await Execute(method, resource, content_, retries).ConfigureAwait(false);
            return string.IsNullOrWhiteSpace(response) ? default(T) : JsonConvert.DeserializeObject<T>(response);
        }

        #endregion


        #region internal

        private async Task<string> Execute(HttpMethod method, string resource, Func<HttpContent> contentDelegate, int retries)
        {
            HttpResponseMessage response = null;
            Exception lastException = null;
            var request = default(HttpRequestMessage);

            do
            {
                try
                {
                    request = new HttpRequestMessage(method, resource);
                    if (contentDelegate != null) request.Content = contentDelegate();

                    await (deleg?.OnBeforeRequest(request) ?? Task.CompletedTask);

                    response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead);
                }
                catch (Exception e)
                {
                    lastException = new EndpointException($"An error occurred while communicating with the endpoint: {httpClient.BaseAddress}/{resource}", e);
                }

                retries--;
            }
            while (retries >= 0 && (response == null || !response.IsSuccessStatusCode));


            if (response == null)
            {
                logger.LogCritical(new EventId(1, "NetworkClient"), lastException, lastException.Message);
                throw lastException;
            }

            var requestUri = request.RequestUri;
            var statusCode = response.StatusCode;
            var reasonPhrase = response.ReasonPhrase;
            var responseHeaders = response.Headers;
            var responseBody = await response.Content.ReadAsStringAsync();
            response.Dispose();
            
            logger.LogTrace($"{statusCode}: {requestUri.AbsoluteUri} \n\n{responseBody}");

            if (!response.IsSuccessStatusCode)
            {
                await (deleg?.OnFailedRequest(requestUri, statusCode, reasonPhrase, responseHeaders, responseBody) ?? Task.CompletedTask);

                if (statusCode == System.Net.HttpStatusCode.NotFound)
                    return null;
                else if (statusCode == System.Net.HttpStatusCode.BadRequest)
                    throw new BadRequestError(statusCode, requestUri, responseBody, deleg?.GetBadRequestMessages(responseBody) ?? new string[] { responseBody });
                else
                    throw new FailedRequestError(statusCode, requestUri, responseBody, reasonPhrase);
            }

            return responseBody;
        }

        protected string Hash(string str)
        {
            string md5Hash;
            using (MD5 hash = MD5.Create())
            {
                md5Hash = string.Join
                (
                    "",
                    from ba in hash.ComputeHash
                    (
                        Encoding.UTF8.GetBytes(str)
                    )
                    select ba.ToString("x2")
                );
            }

            return md5Hash;
        }

        protected string GetCacheKey(HttpMethod method, string resource)
        {
            var id = $"{method}:{httpClient.BaseAddress}{resource}";

            return "NetworkClient:Cache:" + Hash(id);
        }
        
        #endregion

    }
}