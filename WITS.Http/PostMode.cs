﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WITS.Http
{
    public enum PostMode
    {
        FormUrlEncoded,
        Multipart
    }
}
